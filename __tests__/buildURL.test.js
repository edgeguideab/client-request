const clientRequest = require('../src');

it('builds the url from the query option', () => {
  const url = 'https://edgeguide.se';
  const options = {
    query: {
      foo: 'bar'
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe('https://edgeguide.se?foo=bar');
});

it('appends query parameters from the query option to existing parameters', () => {
  const url = 'https://edgeguide.se?fizz=buzz';
  const options = {
    query: {
      foo: 'bar'
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe('https://edgeguide.se?fizz=buzz&foo=bar');
});

it('can append multiple to existing parameters', () => {
  const url = 'https://edgeguide.se?fizz=buzz';
  const options = {
    query: {
      foo: 'bar',
      dead: 'beef',
      hello: 'world'
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe(
    'https://edgeguide.se?fizz=buzz&foo=bar&dead=beef&hello=world'
  );
});

it('encodes values with encodeURIComponent', () => {
  const url = 'https://edgeguide.se';
  const options = {
    query: {
      param: 'hello/world',
      dead: '[beef]'
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe(
    'https://edgeguide.se?param=hello%2Fworld&dead=%5Bbeef%5D'
  );
});

it('encodes keys with encodeURIComponent', () => {
  const url = 'https://edgeguide.se';
  const options = {
    query: {
      param: 'hello/world',
      '[dead]': 'beef'
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe(
    'https://edgeguide.se?param=hello%2Fworld&%5Bdead%5D=beef'
  );
});

it('skips null query parameter values', () => {
  const url = 'https://edgeguide.se';
  const options = {
    query: {
      foo: null
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe('https://edgeguide.se');
});

it('skips undefined query parameter values', () => {
  const url = 'https://edgeguide.se';
  const options = {
    query: {
      foo: undefined
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe('https://edgeguide.se');
});

it('skips undefined query parameter values and includes non-null values', () => {
  const url = 'https://edgeguide.se';
  const options = {
    query: {
      foo: undefined,
      bar: null,
      hello: 'world'
    }
  };

  const fullUrl = clientRequest.buildURL(url, options);
  expect(fullUrl).toBe('https://edgeguide.se?hello=world');
});
