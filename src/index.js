var clientRequest = {};

clientRequest._requestIdentifier = 0;
clientRequest._requestMap = {};

clientRequest.generateRequestHandle = function () {
  clientRequest._requestIdentifier++;
  return clientRequest._requestIdentifier;
};

clientRequest.getRequestObject = function (handle) {
  return clientRequest._requestMap[handle] || null;
};

clientRequest.abortRequest = function (handle) {
  if (!clientRequest._requestMap[handle]) {
    return;
  }
  clientRequest._requestMap[handle].abort();
};

clientRequest.defaultUrl = null;

clientRequest.DEFAULT_TIMEOUT = 60 * 1000;

clientRequest.responseMiddlewares = [];
clientRequest.middleWares = {};

clientRequest.bindResponseMiddleware = function (middleware) {
  if (typeof middleware !== 'function') {
    throw new Error('Middleware needs to be a function');
  }
  clientRequest.responseMiddlewares.push(middleware);
};

clientRequest.bind = function (urlRegexpString, mutator, methods) {
  methods = (Array.isArray(methods) &&
    methods.map(method => method.toUpperCase())) || [
    'GET',
    'POST',
    'PUT',
    'HEAD',
    'DELETE',
    'PATCH'
  ];
  if (typeof methods === 'string') {
    methods = [methods];
  }
  let mutators = clientRequest.middleWares[urlRegexpString] || {};
  methods.forEach(method => (mutators[method] = mutator));

  clientRequest.middleWares[urlRegexpString] = mutators;
};

clientRequest.unbind = function (urlRegexpString, methods) {
  if (!methods) {
    delete clientRequest.middleWares[urlRegexpString];
    return;
  }
  if (typeof clientRequest.middleWares[urlRegexpString] !== 'object') {
    return;
  }

  if (typeof methods === 'string') {
    methods = [methods];
  }

  if (!Array.isArray(methods)) {
    return;
  }
  methods = methods.map(method => method.toUpperCase());

  methods.forEach(function (method) {
    let mutatorsForRegexp = clientRequest.middleWares[urlRegexpString];
    if (mutatorsForRegexp && typeof mutatorsForRegexp[method] === 'function') {
      delete clientRequest.middleWares[urlRegexpString][method];
    }
  });

  if (Object.keys(clientRequest.middleWares[urlRegexpString]).length === 0) {
    delete clientRequest.middleWares[urlRegexpString];
  }
};

clientRequest.executeMiddlewares = async function (url, options, method) {
  url = url || '';
  options = options || {};
  method = method.toUpperCase();
  const middleWarePaths = Object.keys(clientRequest.middleWares);

  for (let i = 0; i < middleWarePaths.length; i++) {
    const pathRegexp = middleWarePaths[i];
    const mutator = clientRequest.middleWares[pathRegexp][method];
    const regexp = new RegExp(pathRegexp.replace('/*/g', '.*'));
    if (regexp.test(url) && typeof mutator === 'function') {
      const result = await mutator({ url, options });
      options = result.options;
      url = result.url;
    }
  }

  return { url, options };
};

clientRequest.buildURL = function (url, options) {
  const query = options.query || {};
  const params = Object.keys(query)
    .filter(key => query[key] !== null && query[key] !== undefined)
    .map(
      key =>
        `${encodeURIComponent(key)}=${encodeURIComponent(options.query[key])}`
    )
    .join('&');

  if (params) {
    var separator = url.indexOf('?') !== -1 ? '&' : '?';
    url += separator + params;
  }
  return url;
};

clientRequest.get = function (url, options) {
  return clientRequest.ajax('GET', url, options);
};
clientRequest.head = function (url, options) {
  return clientRequest.ajax('HEAD', url, options);
};
clientRequest.delete = function (url, options) {
  return clientRequest.ajax('DELETE', url, options);
};
clientRequest.post = function (url, options) {
  return clientRequest.ajax('POST', url, options);
};
clientRequest.patch = function (url, options) {
  return clientRequest.ajax('PATCH', url, options);
};
clientRequest.put = function (url, options) {
  return clientRequest.ajax('PUT', url, options);
};

clientRequest.ajax = async function (method, url, options) {
  options = options || {};
  var modifiedParameters = await clientRequest.executeMiddlewares(
    url,
    options,
    method
  );

  url = modifiedParameters.url;
  options = modifiedParameters.options;
  url = clientRequest.buildURL(url, options);
  return new Promise(function (resolve, reject) {
    options = options || {};
    let data = typeof options.body === 'undefined' ? undefined : options.body;
    let contentType = options.headers
      ? options.headers['Content-Type'] || options.headers['content-type']
      : '';

    if (!url && !clientRequest.defaultUrl) {
      var error = 'No url specified';
      reject({
        error: error
      });
    }
    url = url || clientRequest.defaultUrl;

    var oReq = new XMLHttpRequest();
    if (options.handle) {
      if (clientRequest._requestMap[options.handle]) {
        reject({
          error: `A request with handle id ${options.handle} already exists`
        });
        return;
      }
      clientRequest._requestMap[options.handle] = oReq;
    }
    oReq.open(method, url, true);
    oReq.withCredentials =
      typeof options.withCredentials === 'boolean'
        ? options.withCredentials
        : true;
    if (options.headers) {
      if (
        typeof options.headers === 'object' &&
        !Array.isArray(options.headers)
      ) {
        Object.keys(options.headers).forEach(header =>
          oReq.setRequestHeader(header, options.headers[header])
        );
      }
    }
    if (method !== 'GET' && method !== 'DELETE' && !contentType) {
      switch (Object.prototype.toString.call(data)) {
        case '[object Object]': {
          contentType = 'application/json';
          oReq.setRequestHeader(
            'Content-Type',
            'application/json;charset=UTF-8'
          );
          break;
        }
        case '[object Array]': {
          contentType = 'application/json';
          oReq.setRequestHeader(
            'Content-Type',
            'application/json;charset=UTF-8'
          );
          break;
        }
      }
    }
    if (
      typeof contentType === 'string' &&
      contentType.toLowerCase().indexOf('application/json') !== -1
    ) {
      data = JSON.stringify(data);
    }
    if (oReq.upload) {
      oReq.upload.addEventListener('progress', updateUploadProgress, false);
    }
    oReq.timeout = options.timeout || clientRequest.DEFAULT_TIMEOUT;
    oReq.addEventListener('progress', updateProgress, false);
    oReq.addEventListener('load', transferComplete, false);
    oReq.addEventListener('error', transferFailed, false);
    oReq.addEventListener('abort', transferCanceled, false);
    oReq.addEventListener('timeout', onTimeout, false);
    oReq.responseType = options.responseType || '';
    if (data !== undefined) {
      oReq.send(data);
    } else {
      oReq.send();
    }
    let debug = console.warn ? console.warn : console.log;

    function transferComplete(oEvent) {
      var response = oReq.response;
      try {
        if (
          oReq.getResponseHeader('Content-Type').indexOf('application/json') !==
          -1
        ) {
          response = JSON.parse(response);
        }
      } catch (e) {}
      clientRequest.responseMiddlewares.forEach(middleware => middleware(oReq));
      if (options.handle) {
        delete clientRequest._requestMap[options.handle];
      }
      if (oReq.status >= 400) {
        reject({
          status: oReq.status,
          body: response,
          get error() {
            if (typeof debug !== 'function') {
              return response;
            }
            debug(
              'Getting errors with response.error is deprecated. Use response.body instead'
            );
            return response;
          }
        });
        return;
      }
      resolve({
        event: oEvent,
        originalObject: oReq,
        status: oReq.status,
        get data() {
          debug(
            'Getting data with response.data is deprecated. Use response.body instead'
          );
          return response;
        },
        body: response,
        size: oEvent.total
      });
    }

    function updateProgress(oEvent) {
      if (options.onDataReceived && oEvent.lengthComputable) {
        options.onDataReceived({
          event: oEvent,
          loaded: oEvent.loaded,
          total: oEvent.total
        });
      }
    }

    function updateUploadProgress(oEvent) {
      if (options.onDataUploaded && oEvent.lengthComputable) {
        options.onDataUploaded({
          event: oEvent,
          loaded: oEvent.loaded,
          total: oEvent.total
        });
      }
    }

    function transferFailed(oEvent) {
      if (options.handle) {
        delete clientRequest._requestMap[options.handle];
      }
      reject({
        event: oEvent,
        status: oReq.status,
        body: 'Transfer canceled',
        get error() {
          debug(
            'Getting errors with response.error is deprecated. Use response.body instead'
          );
          return error;
        }
      });
    }

    function transferCanceled(oEvent) {
      if (options.handle) {
        delete clientRequest._requestMap[options.handle];
      }
      reject({
        event: oEvent,
        status: oReq.status,
        body: 'Transfer canceled',
        get error() {
          debug(
            'Getting errors with response.error is deprecated. Use response.body instead'
          );
          return error;
        }
      });
    }

    function onTimeout(oEvent) {
      if (options.handle) {
        delete clientRequest._requestMap[options.handle];
      }
      reject({
        event: oEvent,
        status: 408,
        body: 'Connection to server timed out',
        get error() {
          debug(
            'Getting errors with response.error is deprecated. Use response.body instead'
          );
          return error;
        }
      });
    }
  });
};

try {
  module.exports = clientRequest;
} catch (e) {
  // Not running with webpack/browserify
  console.log('Loaded clientRequest as window.clientRequest');
  window.clientRequest = clientRequest;
}
